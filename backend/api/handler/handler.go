package handler

import (
	"backend/dao"
)

type Handler struct {
	store *dao.Store
}

func NewHandler(store *dao.Store) *Handler {
	return &Handler{
		store: store,
	}
}
