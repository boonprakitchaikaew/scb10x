package handler

import (
	"backend/models"
	"backend/settings"
	"backend/utils"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func (h *Handler) Login(c echo.Context) (err error) {
	var requestBody models.LogIn

	if err = c.Bind(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(401, err.Error()))
	}

	if err = c.Validate(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(401, err.Error()))
	}

	user, err := h.store.User.FindByEmailId(requestBody.Email)

	if err != nil {
		return c.JSON(http.StatusUnauthorized, settings.Errors(401, err.Error()))
	}

	if !utils.ComparePasswords(user.Password, []byte(requestBody.Password)) {
		return c.JSON(http.StatusUnauthorized, settings.Errors(401, ""))
	}

	token, err := createUserToken(user.ID, map[string]string{"email": user.Email})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": token,
	})
}

func (h *Handler) Register(c echo.Context) (err error) {
	var requestBody models.Register

	if err = c.Bind(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(400, err.Error()))
	}

	if err = c.Validate(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(400, err.Error()))
	}

	res, _ := h.store.User.FindByEmailId(requestBody.Email)
	if res.Email != "" {
		return c.JSON(http.StatusBadRequest, settings.Errors(001, "user already exist"))
	}

	if !utils.IsValidPasswordFormat(requestBody.Password) {
		return c.JSON(http.StatusBadRequest, settings.Errors(002, "wrong password format"))
	}

	userIDBytes, _ := uuid.NewRandom()
	user := models.User{
		ID:       userIDBytes.String(),
		Name:     requestBody.Name,
		Image:    requestBody.Image,
		Email:    requestBody.Email,
		Password: utils.HashAndSalt([]byte(requestBody.Password)),
	}

	if err := h.store.User.Insert(user); err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}

	token, err := createUserToken(user.ID, map[string]string{"email": user.Email})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": token,
	})
}

func createUserToken(sub string, data interface{}) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["sub"] = sub
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	claims["data"] = data

	t, err := token.SignedString([]byte(settings.Config.JWT.Secret))
	if err != nil {
		return "", err
	}

	return t, nil
}
