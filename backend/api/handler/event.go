package handler

import (
	"backend/models"
	"backend/settings"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func (h *Handler) CreateEvent(c echo.Context) (err error) {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	sub := claims["sub"].(string)

	var requestBody models.Event

	if err = c.Bind(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(400, err.Error()))
	}

	if err = c.Validate(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(400, err.Error()))
	}

	eventIDBytes, _ := uuid.NewRandom()
	event := models.Event{
		ID:          eventIDBytes.String(),
		Image:       requestBody.Image,
		Title:       requestBody.Title,
		Description: requestBody.Description,
		MaxMember:   requestBody.MaxMember,
		Host:        sub,
		CreatedAt:   time.Now().UTC(),
		Status:      models.EventStattus.ACTIVE,
		Members: map[string]interface{}{
			sub: nil,
		},
	}

	if err := h.store.Event.Insert(event); err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}

	return c.JSON(http.StatusOK, event)
}

func (h *Handler) UpdateEvent(c echo.Context) (err error) {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	sub := claims["sub"].(string)

	var requestBody models.Event

	if err = c.Bind(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(400, err.Error()))
	}

	if err = c.Validate(&requestBody); err != nil {
		return c.JSON(http.StatusBadRequest, settings.Errors(400, err.Error()))
	}

	event, err := h.store.Event.FindById(c.Param("event_id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, settings.Errors(404, err.Error()))
	}

	newEvent := models.Event{
		ID:          event.ID,
		Image:       requestBody.Image,
		Title:       requestBody.Title,
		Description: requestBody.Description,
		MaxMember:   requestBody.MaxMember,
		Host:        sub,
		CreatedAt:   event.CreatedAt,
		Status:      event.Status,
		Members:     event.Members,
	}

	if err := h.store.Event.Update(newEvent); err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}

	return c.JSON(http.StatusOK, event)
}

func (h *Handler) GetEvents(c echo.Context) (err error) {
	events, err := h.store.Event.FindAll()
	if err != nil {
		return c.JSON(http.StatusNotFound, settings.Errors(404, err.Error()))
	}
	return c.JSON(http.StatusOK, events)
}

func (h *Handler) GetEvent(c echo.Context) (err error) {
	event, err := h.store.Event.FindById(c.Param("event_id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, settings.Errors(404, err.Error()))
	}
	return c.JSON(http.StatusOK, event)
}

func (h *Handler) JoinEvent(c echo.Context) (err error) {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	sub := claims["sub"].(string)
	event, err := h.store.Event.FindById(c.Param("event_id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, settings.Errors(404, err.Error()))
	}

	event.Members[sub] = nil

	if len(event.Members) > event.MaxMember {
		return c.JSON(http.StatusOK, settings.Errors(004, "party is not avaible"))
	}

	if err := h.store.Event.Update(event); err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}
	return c.JSON(http.StatusOK, event)
}

func (h *Handler) LeaveEvent(c echo.Context) (err error) {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	sub := claims["sub"].(string)
	event, err := h.store.Event.FindById(c.Param("event_id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, settings.Errors(404, err.Error()))
	}
	delete(event.Members, sub)
	if err := h.store.Event.Update(event); err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}
	return c.JSON(http.StatusOK, event)
}

func (h *Handler) RemoveEvent(c echo.Context) (err error) {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	sub := claims["sub"].(string)

	event, err := h.store.Event.FindById(c.Param("event_id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, settings.Errors(404, err.Error()))
	}

	if event.Host != sub {
		return c.JSON(http.StatusUnauthorized, settings.Errors(403, err.Error()))
	}

	event.Status = models.EventStattus.DELETED
	if err := h.store.Event.Update(event); err != nil {
		return c.JSON(http.StatusInternalServerError, settings.Errors(500, err.Error()))
	}

	return c.NoContent(http.StatusOK)
}
