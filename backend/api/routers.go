package api

import (
	"backend/api/handler"
	"backend/dao"
	"backend/middlewares"
	"backend/models"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
)

func New(store *dao.Store) *echo.Echo {
	e := echo.New()

	e.Validator = &models.Validator{
		Validator: validator.New(),
	}

	h := handler.NewHandler(store)

	restAPI := e.Group("/api")
	restAPI.GET("", func(c echo.Context) (err error) {
		return c.NoContent(http.StatusOK)
	})

	/** =============================================
	set grobal middlewares
	============================================= **/

	middlewares.SetMainMiddlewares(e)
	middlewares.SetCompleteLogMiddlware(e)

	/** =============================================
	user router
	============================================= **/

	users := restAPI.Group("/users")
	users.POST("/login", h.Login)
	users.POST("", h.Register)

	/** =============================================
	event router
	============================================= **/

	isAuthorized := middlewares.IsAuthorized()
	events := restAPI.Group("/events")
	events.GET("", h.GetEvents)
	events.POST("", h.CreateEvent, isAuthorized)
	events.GET("/:event_id", h.GetEvent, isAuthorized)
	events.PUT("/:event_id", h.UpdateEvent, isAuthorized)
	events.DELETE("/:event_id", h.RemoveEvent, isAuthorized)
	events.POST("/:event_id/join", h.JoinEvent, isAuthorized)
	events.POST("/:event_id/leave", h.LeaveEvent, isAuthorized)

	return e
}
