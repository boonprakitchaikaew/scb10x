package middlewares

import (
	"backend/settings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var JWTSigningMethod = "HS256"

func IsAuthorized() echo.MiddlewareFunc {
	return middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: JWTSigningMethod,
		SigningKey:    []byte(settings.Config.JWT.Secret),
		ContextKey:    "user",
	})
}
