package settings

type config struct {
	DataBase struct {
		MongoDBUrl   string `envconfig:"MONGODB_URL"`
		DatabaseName string `envconfig:"DATABASE_NAME"`
	}
	JWT struct {
		Secret string `envconfig:"JWT_SECRET"`
	}
}
