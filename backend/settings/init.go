package settings

import (
	"fmt"
	"os"

	"github.com/kelseyhightower/envconfig"
)

var Config config

func Init() {
	err := envconfig.Process("", &Config)
	if err != nil {
		processError(err)
	}
}

func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}
