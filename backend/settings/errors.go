package settings

type Error struct {
	Status  int    `bson:"status" json:"status"`
	Message string `bson:"message" json:"message"`
	Detail  string `bson:"detail,omitempty" json:"detail,omitempty"`
	Field   string `bson:"field,omitempty" json:"field,omitempty"`
}

var errors = map[int]Error{
	500: {
		Status:  500,
		Message: "Internal server error",
	},
	400: {
		Status:  400,
		Message: "Bad request",
	},
	401: {
		Status:  401,
		Message: "Unauthorized",
	},
	403: {
		Status:  403,
		Message: "Forbidden",
	},
	404: {
		Status:  404,
		Message: "Not found",
	},
	001: {
		Status:  001,
		Message: "Email already exist",
		Field:   "email",
	},
	002: {
		Status:  002,
		Message: "Password should has more than 7 charactors",
		Field:   "password",
	},
	004: {
		Status:  004,
		Message: "party is not available",
	},
}

// Errors --
func Errors(code int, detail string) Error {
	error := errors[code]
	error.Detail = detail
	return error
}
