package dao

import (
	"backend/models"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type eventStore struct {
	db *mgo.Database
}

func newEventStore(db *mgo.Database) *eventStore {
	return &eventStore{
		db: db,
	}
}

func (store *eventStore) FindAll() ([]models.Event, error) {
	pipeline := []bson.M{
		{"$match": bson.M{
			"status": models.EventStattus.ACTIVE,
		}},
	}

	pipe := store.db.C("event").Pipe(pipeline)
	result := []models.Event{}
	err := pipe.All(&result)
	return result, err
}

func (store *eventStore) FindById(id string) (models.Event, error) {
	var event models.Event
	err := store.db.C("event").Find(bson.M{"_id": id}).One(&event)
	return event, err
}

func (store *eventStore) FindByAppId(id string) (models.Event, error) {
	var event models.Event
	err := store.db.C("event").Find(bson.M{"app_id": id}).One(&event)
	return event, err
}

func (store *eventStore) Insert(event models.Event) error {
	err := store.db.C("event").Insert(&event)
	return err
}

func (store *eventStore) Update(event models.Event) error {
	err := store.db.C("event").UpdateId(event.ID, &event)
	return err
}
