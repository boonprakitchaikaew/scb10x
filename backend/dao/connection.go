package dao

import (
	"backend/settings"
	"fmt"

	"gopkg.in/mgo.v2"
)

type Store struct {
	User  *userStore
	Event *eventStore
}

func Connect() *Store {
	session, err := mgo.Dial(settings.Config.DataBase.MongoDBUrl)
	if err != nil {
		fmt.Println("Connect Mongo DB error")
		fmt.Println(err.Error())
	}

	var db *mgo.Database
	db = session.DB(settings.Config.DataBase.DatabaseName)
	return &Store{
		User:  newUserStore(db),
		Event: newEventStore(db),
	}
}
