package dao

import (
	"backend/models"
	"fmt"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type userStore struct {
	db *mgo.Database
}

func newUserStore(db *mgo.Database) *userStore {
	return &userStore{
		db: db,
	}
}

func (us *userStore) FindByEmailId(email string) (models.User, error) {
	var user models.User
	err := us.db.C("user").Find(bson.M{"email": email}).One(&user)
	return user, err
}

func (us *userStore) CreateEmailIndex() {
	err := us.db.C("user").DropIndex("email_text")
	if err != nil {
		fmt.Println("email_text index doesn't exist")
	}

	index := mgo.Index{
		Key: []string{"$text:email"},
	}

	if err := us.db.C("user").EnsureIndex(index); err != nil {
		panic(err)
	}
}

func (us *userStore) FindById(id string) (models.User, error) {
	var user models.User
	err := us.db.C("user").Find(bson.M{"_id": id}).One(&user)
	return user, err
}

func (us *userStore) Insert(user models.User) error {
	err := us.db.C("user").Insert(&user)
	return err
}

func (us *userStore) Update(user models.User) error {
	err := us.db.C("user").UpdateId(user.ID, &user)
	return err
}
