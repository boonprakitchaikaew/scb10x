package models

type (
	LogIn struct {
		Email    string `bson:"email" json:"email" validate:"required,email"`
		Password string `bson:"password,omitempty" json:"password,omitempty" validate:"required"`
	}

	Register struct {
		Image    string `bson:"image" json:"image"`
		Name     string `bson:"name" json:"name" validate:"required"`
		Email    string `bson:"email" json:"email" validate:"required,email"`
		Password string `bson:"password,omitempty" json:"password,omitempty" validate:"required"`
	}

	User struct {
		ID       string `bson:"_id" json:"_id"`
		Name     string `bson:"name" json:"name"`
		Image    string `bson:"image" json:"image"`
		Email    string `bson:"email" json:"email"`
		Password string `bson:"password,omitempty" json:"password,omitempty"`
	}
)
