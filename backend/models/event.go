package models

import (
	"time"
)

type (
	Event struct {
		ID          string                 `bson:"_id" json:"_id"`
		Image       string                 `bson:"image" json:"image"`
		Title       string                 `bson:"title" json:"title" validate:"required"`
		Description string                 `bson:"description" json:"description" validate:"required"`
		MaxMember   int                    `bson:"max_member" json:"max_member" validate:"required"`
		Members     map[string]interface{} `bson:"members" json:"members"`
		Host        string                 `bson:"user_id" json:"user_id"`
		Status      string                 `bson:"status" json:"status"`
		CreatedAt   time.Time              `bson:"created_at" json:"created_at"`
	}

	CreateEvent struct {
		Image       string `bson:"image" json:"image"`
		Title       string `bson:"title" json:"title" validate:"required"`
		Description string `bson:"description" json:"description" validate:"required"`
		MaxMember   int    `bson:"max_member" json:"max_member" validate:"required"`
	}

	UpdateEvent struct {
		Image       string `bson:"image" json:"image"`
		Title       string `bson:"title" json:"title" validate:"required"`
		Description string `bson:"description" json:"description" validate:"required"`
		MaxMember   int    `bson:"max_member" json:"max_member" validate:"required"`
	}

	eventStatus struct {
		ACTIVE  string
		DELETED string
	}
)

var EventStattus eventStatus = eventStatus{
	ACTIVE:  "active",
	DELETED: "deletetd",
}
