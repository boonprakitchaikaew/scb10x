package main

import (
	"backend/api"
	"backend/dao"
	"backend/settings"
)

var store *dao.Store

func init() {
	settings.Init()

	store = dao.Connect()
}

func main() {
	rest := api.New(store)
	rest.Start(":80")
}
