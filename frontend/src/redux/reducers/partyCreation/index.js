import { default as types } from '../../types'

const defaultState = {
    image: null,
    title: '',
    description: '',
    maxNumber: 5
}

const partyCreation = (state = defaultState, action) => {
    switch (action.type) {
        case types.partyCreation.ON_IMAGE_CHANGED:
            return {
                ...state,
                image: action.payload.image
            }
        case types.partyCreation.ON_TITLE_CHANGED:
            return {
                ...state,
                title: action.payload.title
            }
        case types.partyCreation.ON_DESCRIPTION_CHANGED:
            return {
                ...state,
                description: action.payload.description
            }
        case types.partyCreation.ON_MAX_MEMBER_CHANGED:
            return {
                ...state,
                maxMember: action.payload.maxMember
            }
        case types.partyCreation.IS_CREATING:
            return {
                ...state,
                isCreating: action.payload.status
            }
        default:
            return state
    }
}

export default partyCreation