import { combineReducers } from 'redux'
import login from './login'
import partyCreation from './partyCreation'
import partyList from './partyList'
import register from './register'
import system from './system'

export default combineReducers({
    login,
    register,
    partyCreation,
    partyList,
    system
})
