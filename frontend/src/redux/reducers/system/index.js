import { default as types } from '../../types'

const defaultState = {
    user: {}
}

const system = (state = defaultState, action) => {
    switch (action.type) {
        case types.system.USER:
            return {
                ...state,
                user: action.payload.user
            }
        default:
            return state
    }
}

export default system