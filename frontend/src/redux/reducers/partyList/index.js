import { default as types } from '../../types'

const defaultState = {
    events: {}
}

const partyList = (state = defaultState, action) => {
    switch (action.type) {
        case types.partyList.FETCH_EVENTS_RESULT:
            return {
                ...state,
                events: action.payload.result
            }
        case types.partyList.IS_INITING:
            return {
                ...state,
                isIniting: action.payload.status
            }
        default:
            return state
    }
}

export default partyList