import { default as types } from '../../types'

const defaultState = {
    email: '',
    name: '',
    password: '',
    condition1: false,
    condition2: false
}

const register = (state = defaultState, action) => {
    switch (action.type) {
        case types.register.ON_REGISTER_EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload.email
            }
        case types.register.ON_REGISTER_PASSWORD_CHANGED:
            return {
                ...state,
                password: action.payload.password
            }
        case types.register.ON_REGISTER_NAME_CHANGED:
            return {
                ...state,
                name: action.payload.name
            }
        case types.register.ON_ERROR:
            return {
                ...state,
                error: action.payload.error
            }
        case types.register.IS_REGISTERING_IN:
            return {
                ...state,
                isRegistering: action.payload.status
            }
        case types.register.NAME_ERROR:
            return {
                ...state,
                nameError: action.payload.error
            }
        case types.register.EMAIL_ERROR:
            return {
                ...state,
                emailError: action.payload.error
            }
        case types.register.PASSWORD_ERROR:
            return {
                ...state,
                passwordError: action.payload.error
            }
        default:
            return state
    }
}

export default register