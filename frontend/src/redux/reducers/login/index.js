import { default as types } from '../../types'

const defaultState = {
    email: '',
    password: '',
    error: null
}

const login = (state = defaultState, action) => {
    switch (action.type) {
        case types.login.ON_LOGIN_EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload.email
            }
        case types.login.ON_LOGIN_PASSWORD_CHANGED:
            return {
                ...state,
                password: action.payload.password
            }
        case types.login.ON_ERROR:
            return {
                ...state,
                error: action.payload.error
            }
        case types.login.IS_LOGGING_IN:
            return {
                ...state,
                isLoggingIn: action.payload.status
            }
        default:
            return state
    }
}

export default login