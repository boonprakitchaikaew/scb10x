import {default as types}  from '../../types'

export const fetchEventResult = (result) => ({
    type: types.partyList.FETCH_EVENTS_RESULT,
    payload: {
        result
    }
})

export const fetchEvents = (password) => ({
    type: types.partyList.FETCH_EVENTS
})

export const joinEvent = (event) => ({
    type: types.partyList.JOIN_EVENT,
    payload: {
        event
    }
})

export const leaveEvent = (event) => ({
    type: types.partyList.LEAVE_EVENT,
    payload: {
        event
    }
})

export const isIniting = (status) => ({
    type: types.partyList.IS_INITING,
    payload: {
        status
    }
})