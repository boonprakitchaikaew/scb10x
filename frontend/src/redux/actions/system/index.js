import { default as types } from '../../types'

export const startApp = () => ({
    type: types.system.START_APP
})

export const user = (user) => ({
    type: types.system.USER,
    payload: {
        user
    }
})