import {default as types}  from '../../types'

export const onLoginClicked = () => ({
    type: types.login.ON_LOGIN_CLICKED
})

export const onLoginEmailChanged = (email) => ({
    type: types.login.ON_LOGIN_EMAIL_CHANGED,
    payload: {
        email
    }
})

export const onLoginPasswordChanged = (password) => ({
    type: types.login.ON_LOGIN_PASSWORD_CHANGED,
    payload: {
        password
    }
})

export const onError = (error) => ({
    type: types.login.ON_ERROR,
    payload: {
        error
    }
})

export const isLoggingIn = (status) => ({
    type: types.login.IS_LOGGING_IN,
    payload: {
        status
    }
})