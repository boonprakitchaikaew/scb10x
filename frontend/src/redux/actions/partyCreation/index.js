import { default as types } from '../../types'

export const onCreatePartyClicked = () => ({
    type: types.partyCreation.ON_CREATE_PARTY_CLICKED
})

export const onImageChanged = (image) => ({
    type: types.partyCreation.ON_IMAGE_CHANGED,
    payload: {
        image
    }
})

export const onTitleChanged = (title) => ({
    type: types.partyCreation.ON_TITLE_CHANGED,
    payload: {
        title
    }
})

export const onDescriptionChanged = (description) => ({
    type: types.partyCreation.ON_DESCRIPTION_CHANGED,
    payload: {
        description
    }
})

export const onMaxMemberChanged = (maxMember) => ({
    type: types.partyCreation.ON_MAX_MEMBER_CHANGED,
    payload: {
        maxMember
    }
})

export const isCreating = (status) => ({
    type: types.partyCreation.IS_CREATING,
    payload: {
        status
    }
})
