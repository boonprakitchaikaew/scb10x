import {default as types}  from '../../types'

export const onRegisterClicked = () => ({
    type: types.register.ON_REGISER_CLICKED
})

export const onRegisterEmailChanged = (email) => ({
    type: types.register.ON_REGISTER_EMAIL_CHANGED,
    payload: {
        email
    }
})

export const onRegiserPasswordChanged = (password) => ({
    type: types.register.ON_REGISTER_PASSWORD_CHANGED,
    payload: {
        password
    }
})

export const onRegiserNameChanged = (name) =>({
    type: types.register.ON_REGISTER_NAME_CHANGED,
    payload: {
        name
    }
})

export const onError = (error) => ({
    type: types.register.ON_ERROR,
    payload: {
        error
    }
})

export const isRegistering = (status) => ({
    type: types.register.IS_REGISTERING_IN,
    payload: {
        status
    }
})

export const nameError = (error) => ({
    type: types.register.NAME_ERROR,
    payload: {
        error
    }
})

export const emailError = (error) => ({
    type: types.register.EMAIL_ERROR,
    payload: {
        error
    }
})

export const passwordError = (error) => ({
    type: types.register.PASSWORD_ERROR,
    payload: {
        error
    }
})