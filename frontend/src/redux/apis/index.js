import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:5000'
axios.interceptors.request.use(function (config) {
    const token = window.localStorage.getItem('token')
    config.headers.Authorization = `Bearer ${token}`
    return config
}, function (error) {
    return Promise.reject(error)
})

export const login = (email, password) => {
    return axios.post(`/api/users/login`, {
        email,
        password
    })
}

export const register = (name, email, password) => {
    return axios.post(`/api/users`, {
        name,
        email,
        password
    })
}

export const events = () => {
    return axios.get(`/api/events`)
}

export const joinEvent = (id) => {
    return axios.post(`/api/events/${id}/join`, {})
}

export const leaveEvent = (id) => {
    return axios.post(`/api/events/${id}/leave`, {})
}

export const createParty = (image, title, description, maxMember) => {
    return axios.post(`/api/events`, {
        image: 'fs',
        title,
        description,
        max_member: maxMember
    })
}