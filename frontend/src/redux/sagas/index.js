import { all } from 'redux-saga/effects'
import createEventClicked from './partyCreation/createEventClicked'
import fetchEvents from './partyList/fetchEvents'
import joinEvent from './partyList/joinEvent'
import leaveEvent from './partyList/leaveEvent'
import loginClicked from './login/loginClicked'
import registerClicked from './register/registerClicked'
import startApp from './system/startApp'

// single entry point to start all Sagas at once
export default function* rootSaga() {
    yield all([
        startApp(),
        createEventClicked(),
        loginClicked(),
        fetchEvents(),
        registerClicked(),
        joinEvent(),
        leaveEvent()
    ])
}
