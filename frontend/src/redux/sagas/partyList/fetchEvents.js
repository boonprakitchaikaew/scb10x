import * as actions from '../../actions/partyList'
import * as apis from '../../apis'

import { call, put, select, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* fetchEvents() {
    while (true) {
        yield take(types.partyList.FETCH_EVENTS)
        try {
            yield put(actions.isIniting(true))
            const { data } = yield call(apis.events)
            const events = data.reduce((o, n) => {
                return {
                    ...o,
                    [n._id]: n
                }
            }, {})
            yield put(actions.fetchEventResult(events))
            yield put(actions.isIniting(false))
        } catch (e) {
            console.warn('[fetchEvents] ' ,e)
        }
    }
}

export default fetchEvents