import * as actions from '../../actions/partyList'
import * as apis from '../../apis'
import * as selectors from '../../selectors/partyList'

import { call, put, select, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* leaveEvent() {
    while (true) {
        const { payload: { event }} = yield take(types.partyList.LEAVE_EVENT)
        try {
            const events = yield select(selectors.getEvents)
            const { data } = yield call(apis.leaveEvent, event._id)
            events[event._id] = data
            yield put(actions.fetchEventResult({
                ...events
            }))
        } catch (e) {
            console.warn('[leaveEvent] ', e)
        }
    }
}

export default leaveEvent