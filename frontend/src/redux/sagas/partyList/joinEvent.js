import * as actions from '../../actions/partyList'
import * as apis from '../../apis'
import * as selectors from '../../selectors/partyList'

import { call, put, select, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* joinEvent() {
    while (true) {
        const { payload: { event }} = yield take(types.partyList.JOIN_EVENT)
        try {
            if(window.localStorage.getItem('token')) {
                const events = yield select(selectors.getEvents)
                const { data } = yield call(apis.joinEvent, event._id)

                // status from backend, it mean party is not available
                if(data.status == 4) {
                    events[event._id].isFull = true
                    yield put(actions.fetchEventResult({
                        ...events
                    }))
                } else {
                    events[event._id] = data
                    yield put(actions.fetchEventResult({
                        ...events
                    }))
                }
                continue
            } 

            window.location.href = `/login?redirect=${window.location.href}`
        } catch (e) {
            console.warn('[joinEvent] ' ,e)
        }
    }
}

export default joinEvent