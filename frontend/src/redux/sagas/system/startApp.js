import * as actions from '../../actions/system'

import { put, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* startApp() {
    while (true) {
        yield take(types.system.START_APP)
        try {
            console.info('start app')
            const token = localStorage.getItem('token')
            const user = JSON.parse(atob(token.split('.')[1]))
            yield put(actions.user(user))
        } catch (e) {
            console.warn('[startApp] ' ,e)
        }
    }
}

export default startApp