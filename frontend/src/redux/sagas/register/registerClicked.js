import * as actions from '../../actions/register'
import * as apis from '../../apis'
import * as selectors from '../../selectors/register'

import { call, put, select, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* onRegisterClicked() {
    while (true) {
        yield take(types.register.ON_REGISER_CLICKED)
        try {
            yield put(actions.onError(null))
            yield put(actions.nameError(null))
            yield put(actions.emailError(null))
            yield put(actions.passwordError(null))

            const name = yield select(selectors.getName)
            if(!name) {
                yield put(actions.emailError('กรุณากรอกชื่อผู้ใช้'))
            }
            yield put(actions.isRegistering(true))
            const email = yield select(selectors.getEmail)
            const password = yield select(selectors.getPassword)
            const { data } = yield call(apis.register, name, email, password)
            localStorage.setItem('token', data.token)
            yield put(actions.isRegistering(false))
        } catch (e) {
            const { status } = e.response.data
            yield put(actions.isRegistering(false))
            if(status == 1) {
                yield put(actions.emailError('อีเมลนี้ถูกใช้งานแล้ว'))
                continue
            } else if(status == 2) {
                yield put(actions.passwordError('กรุณาใช้รหัสความยาวเกิน 8 ตัวอักษร'))
                continue
            }
            yield put(actions.onError('เกิดข้อผิดพลาดขึ้น กรุณาลองอีกคัร้ง'))
            console.warn('[onRegisterClicked] ' ,e)
        }
    }
}

export default onRegisterClicked