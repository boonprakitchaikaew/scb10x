import * as actions from '../../actions/login'
import * as apis from '../../apis'
import * as selectors from '../../selectors/login'

import { call, put, select, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* onLoginClicked() {
    while (true) {
        yield take(types.login.ON_LOGIN_CLICKED)
        try {
            yield put(actions.onError(null))
            yield put(actions.isLoggingIn(true))
            const email = yield select(selectors.getEmail)
            const password = yield select(selectors.getPassword)
            const { data } = yield call(apis.login, email, password)
            localStorage.setItem('token', data.token)
            yield put(actions.isLoggingIn(false))
            const params = new URLSearchParams(window.location.search)
            window.location.href = params.get('redirect')
        } catch (e) {
            yield put(actions.isLoggingIn(false))
            yield put(actions.onError('อีเมลหรือรหัสของคุณไม่ถูกต้อง'))
            console.warn('[onLoginClicked] ', e)
        }
    }
}

export default onLoginClicked