import * as actions from '../../actions/partyCreation'
import * as apis from '../../apis'
import * as selectors from '../../selectors/partyCreation'

import { call, put, select, take } from 'redux-saga/effects'

import {default as types}  from '../../types'

function* onCreateEventClicked() {
    while (true) {
        yield take(types.partyCreation.ON_CREATE_PARTY_CLICKED)
        try {
            yield put(actions.isCreating(true))
            const image = yield select(selectors.getImage)
            const title = yield select(selectors.getTitle)
            const description = yield select(selectors.getDescription)
            const maxMember = yield select(selectors.getMaxMember)
            const { data } = yield call(apis.createParty, image, title, description, parseInt(maxMember))
            window.location.href = '/'
            yield put(actions.isCreating(false))
        } catch (e) {
            yield put(actions.isCreating(false))
            console.warn('[onCreateEventClicked] ' ,e)
        }
    }
}

export default onCreateEventClicked