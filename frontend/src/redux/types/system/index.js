const START_APP = 'system/START_APP'
const USER = 'system/USER'

export default {
    START_APP,
    USER
}