import { default as login } from './login'
import { default as partyCreation } from './partyCreation'
import { default as partyList } from './partyList'
import { default as register } from './register'
import { default as system } from './system'

export default {
    system,
    login,
    register,
    partyCreation,
    partyList
}

