export const getEmail = (state) => {
    return state.register.email
}

export const getPassword = (state) => {
    return state.register.password
}

export const getName = (state) => {
    return state.register.name
}
