export const getEmail = (state) => {
    return state.login.email
}

export const getPassword = (state) => {
    return state.login.password
}
