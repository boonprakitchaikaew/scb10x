export const getImage = (state) => {
    return state.partyCreation.image
}

export const getTitle = (state) => {
    return state.partyCreation.title
}

export const getDescription = (state) => {
    return state.partyCreation.description
}

export const getMaxMember = (state) => {
    return state.partyCreation.maxMember
}