import './style.scss'

import * as actions from '../../redux/actions/login'

import React, { Component } from 'react'

import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

class Login extends Component {
    render() {
        const { 
            onLoginClicked, 
            onEmailChanged, 
            onPasswordChanged,
            email,
            password,
            error,
            isLoggingIn
        } = this.props
        return  (
            <div className='login-container'>
                <h1>เข้าสู่ระบบ</h1>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    if(isLoggingIn) return
                    onLoginClicked()
                }}>
                    <div className='content'>
                        <div className='input-container'>
                            <label>อีเมล</label>
                            <input type='email' required={true} onChange={onEmailChanged} value={email} />
                        </div>
                        <div className='input-container'>
                            <label>รหัสผ่าน</label>
                            <input type='password' required={true} onChange={onPasswordChanged} value={password} />
                        </div>
                        {
                            true && <div className='error-container' >
                                <p>มีข้อผิดพลาดเกิดขึ้น</p>
                            </div>
                        }
                        {
                            !isLoggingIn && <button type='submit'>เข้าสู่ระบบ</button>
                        }
                        {
                            isLoggingIn && <button>กำลังดำเนินการ</button>
                        }
                    </div>
                </form>
                <NavLink exact to={`/join`}>
                    สร้างบัญชีผู้ใช้
                </NavLink>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    email: state.login.email,
    password: state.login.password,
    error: state.login.error,
    isLoggingIn: state.login.isLoggingIn
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onLoginClicked: () => {
        dispatch(actions.onLoginClicked())
    },
    onEmailChanged: (e) => {
        dispatch(actions.onLoginEmailChanged(e.target.value))
    },
    onPasswordChanged: (e) => {
        dispatch(actions.onLoginPasswordChanged(e.target.value))
    }
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)
    
export default ReduxWrapper

