import './style.scss'

import React, { Component } from 'react'

import { connect } from 'react-redux'

class NotFound extends Component {
    render() {
        return  (
            <div className='404-container'>
                404-container
            </div>
        )
    }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch, ownProps) => ({
   
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(NotFound)
    
export default ReduxWrapper

