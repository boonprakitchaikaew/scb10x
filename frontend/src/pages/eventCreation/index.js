import './style.scss'

import * as actions from '../../redux/actions/partyCreation'

import React, { Component } from 'react'

import { connect } from 'react-redux'

class EventCreation extends Component {
    render() {
        const { 
            onCreateEventClicked, 
            onImageChanged, 
            onTitleChanged,
            onDescriptionChanged,
            onMaxMemberChanged,
            title,
            description,
            maxMember
        } = this.props

        return  (
            <div className='event-creation-container'>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    onCreateEventClicked()
                }}>
                    <input type='file' required={true} onChange={onImageChanged} />
                    <input type='text' required={true} onChange={onTitleChanged} value={title} />
                    <input type='text' required={true} onChange={onDescriptionChanged} value={description} />
                    <input type='number' required={true} onChange={onMaxMemberChanged} value={maxMember} />
                    <button type='submit'>Submit</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onCreateEventClicked: () => {
        dispatch(actions.onCreateEventClicked())
    },
    onImageChanged: (e) => {
        dispatch(actions.onTitleChanged(e.target.value))
    },
    onTitleChanged: (e) => {
        dispatch(actions.onTitleChanged(e.target.value))
    },
    onDescriptionChanged: (e) => {
        dispatch(actions.onDescriptionChanged(e.target.value))
    },
    onMaxMemberChanged: (e) => {
        dispatch(actions.onMaxMemberChanged(e.target.value))
    }
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(EventCreation)
    
export default ReduxWrapper

