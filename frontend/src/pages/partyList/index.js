import './style.scss'

import * as actions from '../../redux/actions/partyList'

import React, { Component } from 'react'

import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

class PartyList extends Component {
    componentDidMount = () => {
        const { fetchEvents } = this.props
        fetchEvents()
    }

    render() {
        const { isIniting, events, joinEvent, leaveEvent, user } = this.props

        if(isIniting) {
            return (<div>loading</div>)
        }

        return  (
            <div className='party-list-container'>
                <NavLink exact to={user.sub ? `/create-party` : `/login?redirect=${window.location.origin}/create-party` }>
                    สร้างปาร์ตี้
                </NavLink>
                {
                    Object.values(events).map((event) => {
                        return (<div className='event-container'>
                            <img src={event.image} width='100%' />
                            <p>{event.title}</p>
                            <p>{event.description}</p>
                            <p>{event.isFull ? event.max_member : Object.keys(event.members).length} / {event.max_member}</p>
                            <p>{event.name}</p>
                            {
                                !event.isFull && !event.members.hasOwnProperty(user.sub) && <button onClick={() => joinEvent(event)}>เข้าร่วมปาร์ตี้</button>
                            }
                            {
                                !event.isFull && event.members.hasOwnProperty(user.sub) && <button onClick={() => leaveEvent(event)}>ออกจากปาร์ตี้</button>
                            }
                            {
                                event.isFull && <div>
                                    <button disabled={true}>เต็มแล้ว</button>
                                    <p>**ไม่สามารถเข้าร่วมได้ ปาร์ตี้เต็มแล้ว</p>
                                </div>
                            }
                        </div>)
                    })
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        events: state.partyList.events,
        isIniting: state.partyList.isIniting,
        user: state.system.user
    }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    fetchEvents: () => {
        dispatch(actions.fetchEvents())
    },
    joinEvent: (event) => {
        dispatch(actions.joinEvent(event))
    },
    leaveEvent: (event) => {
        dispatch(actions.leaveEvent(event))
    }
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(PartyList)
    
export default ReduxWrapper

