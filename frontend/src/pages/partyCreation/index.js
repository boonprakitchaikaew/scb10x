import './style.scss'

import * as actions from '../../redux/actions/partyCreation'

import React, { Component } from 'react'

import { connect } from 'react-redux'

class PartyCreation extends Component {
    render() {
        const { 
            onCreatePartyClicked,
            onImageChanged,
            onNameChanged,
            onDescriptionChanged,
            onMaxMemberChanged,
            image,
            name,
            description,
            maxMember,
            error,
            isCreating
        } = this.props

        return  (
            <div className='register-container'>
                <form 
                    onSubmit={(e) => {
                        e.preventDefault()
                        onCreatePartyClicked()
                    }}
                >
                    <div className='content'>
                        <h1>สร้างปาร์ตี้</h1>
                        <div className='image-container'>
                            <label>รูป</label>
                            <input type='file' required={true} onChange={onImageChanged} value={image} />
                        </div>
                        <div className='name-container'>
                            <label>ชื่อปาร์ตี้</label>
                            <input type='text' required={true} onChange={onNameChanged} value={name} />
                        </div>
                        <div className='description-container'>
                            <label>รายละเอียด</label>
                            <input type='text' required={true} onChange={onDescriptionChanged} value={description} />
                        </div>
                        <div className='password-container'>
                            <label>จำนวนสมาชิก</label>
                            <input type='number' min={2} required={true} onChange={onMaxMemberChanged} value={maxMember} />
                        </div>
                        {
                            !isCreating && <button type='submit'>สร้าง</button>
                        }
                        {
                            isCreating && <button>กำลังดำเนินการ</button>
                        }
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    image: state.partyCreation.image,
    name: state.partyCreation.name,
    description: state.partyCreation.description,
    maxMember: state.partyCreation.maxMember,
    isCreating: state.partyCreation.isCreating
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onCreatePartyClicked: () => {
        dispatch(actions.onCreatePartyClicked())
    },
    onImageChanged: (e) => {
        dispatch(actions.onTitleChanged(e.target.value))
    },
    onTitleChanged: (e) => {
        dispatch(actions.onTitleChanged(e.target.value))
    },
    onDescriptionChanged: (e) => {
        dispatch(actions.onDescriptionChanged(e.target.value))
    },
    onMaxMemberChanged: (e) => {
        dispatch(actions.onMaxMemberChanged(e.target.value))
    }
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(PartyCreation)
    
export default ReduxWrapper
