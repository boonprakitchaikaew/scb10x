import './style.scss'

import * as actions from '../../redux/actions/register'

import React, { Component } from 'react'

import { connect } from 'react-redux'

class Register extends Component {
    render() {
        const { 
            onRegisterClicked, 
            onEmailChanged, 
            onPasswordChanged,
            onNameChanged,
            name,
            email,
            password,
            error,
            isRegistering,
            nameError,
            emailError,
            passwordError
        } = this.props
        return  (
            <div className='register-container'>
                <form 
                    onSubmit={(e) => {
                        e.preventDefault()
                        onRegisterClicked()
                    }}
                >
                    <div className='content'>
                        <h1>สร้างบัญชีผู้ใช้</h1>
                        <div className='name-container'>
                            <label>ชื่อผู้ใช้งาน</label>
                            <input type='text' required={true} onChange={onNameChanged} value={name} />
                            {
                                nameError && <p>กรุณากรอกชื่อผู้ใช้</p>
                            }
                        </div>
                        <div className='email-container'>
                            <label>อีเมล</label>
                            <input type='text' required={true} onChange={onEmailChanged} value={email} />
                            {
                                emailError && <p>อีเมลนี้ถูกใช้งานแล้ว</p>
                            }
                        </div>
                        <div className='password-container'>
                            <label>รหัสผ่าน</label>
                            <input type='password' required={true} onChange={onPasswordChanged} value={password} />
                            {
                                passwordError && <p>กรุณาใช้รหัสความยาวเกิน 8 ตัวอักษร</p>
                            }
                        </div>
                        {
                            error && <div className='error-container' >
                                <p>{ error }</p>
                            </div>
                        }
                        {
                            !isRegistering && <button type='submit'>เข้าสู่ระบบ</button>
                        }
                        {
                            isRegistering && <button>กำลังดำเนินการ</button>
                        }
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    email: state.register.email,
    name: state.register.name,
    password: state.register.password,
    nameError: state.register.nameError,
    emailError: state.register.emailError,
    passwordError: state.register.passwordError
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onRegisterClicked: () => {
        dispatch(actions.onRegisterClicked())
    },
    onEmailChanged: (e) => {
        dispatch(actions.onRegisterEmailChanged(e.target.value))
    },
    onPasswordChanged: (e) => {
        dispatch(actions.onRegiserPasswordChanged(e.target.value))
    },
    onNameChanged: (e) => {
        dispatch(actions.onRegiserNameChanged(e.target.value))
    }
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(Register)
    
export default ReduxWrapper

