import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import Login from './pages/login'
import NotFound from './pages/404'
import PartyCreation from './pages/partyCreation'
import PartyList from './pages/partyList'
import Register from './pages/register'
import { connect } from 'react-redux'
import { startApp } from './redux/actions/system'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: localStorage.getItem('token')
        }
    }
    
    render = () => {
        const { token } = this.state
        return  (
            <div className='app-container'>
                <Switch>
                    <Route 
                        exact 
                        path='/join' 
                        render={() => {
                            if(token) {
                                return (<Redirect to='' />)
                            }
                            return (<Register />)
                        }} 
                    />
                    <Route 
                        exact 
                        path='/login' 
                        render={() => {
                            if(token) {
                                return (<Redirect to='' />)
                            }
                            return (<Login />)
                        }} 
                    />
                    <Route 
                        path='/create-party' 
                        render={() => {
                            if(!token) {
                                return (<Redirect to='/login' />)
                            }
                            return (<PartyCreation />)
                        }} 
                    />
                    <Route 
                        path='/' 
                        render={() => {
                            return (<PartyList />)
                        }} 
                    />
                    <Route  
                        exact path='/404' 
                        render={() => {
                            return (<NotFound />)
                        }} 
                    />
                    <Redirect from='*' to='/' />
                </Switch>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    startApp: () => {
        dispatch(startApp())
    }
})
    
const ReduxWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
    
export default ReduxWrapper

