db.event.drop()
db.event.insertMany([
    {
        "_id": "event-1",
        "image": "https://notebookspec.com/web/wp-content/uploads/2020/03/jrjigttgtig.png",
        "title": "หาคนหาร netflix",
        "description": "คนละ 110 บาท หาร 4คน",
        "max_member": 5,
        "members": {
            "user-1": null 
        },
        "user_id": "user-1",
        "status": "active",
        "created_at": "2020-06-19T08:35:41.490Z"
    },
    {
        "_id": "event-2",
        "image": "https://chillchilljapan.com/wp-content/uploads/2018/07/39509075231_9884700eb4_z.jpg",
        "title": "กินโอมากะเสะ หาร 2",
        "description": "กินโอมากะเสะ 4000 มา 2 จ่าย 1",
        "max_member": 2,
        "members": {
            "user-1": null 
        },
        "user_id": "user-1",
        "status": "active",
        "created_at": "2020-06-19T08:35:41.490Z"
    }
])

db.user.insert({
    "_id": "user-1",
    "name": "testuser",
    "image": "https://notebookspec.com/web/wp-content/uploads/2020/03/jrjigttgtig.png",
    "email": "testuser@scb10x.com",
    "password": "$2a$04$oCyR13iWsPJTl4RRhsAopeV3HB3HTW9aOhIvG88ClK33s2zowNnYm"
})
